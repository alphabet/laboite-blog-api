module.exports = {
  extends: '@loopback/eslint-config',
  rules: {
    '@typescript-eslint/no-unused-vars': ['off'],
    '@typescript-eslint/prefer-nullish-coalescing': ['off'],
    // Note: you must disable the base rule as it can report incorrect errors
  },
};
